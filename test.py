from Sky import Sky

# create sky
x = Sky(
	dimensions=3, 
	interval=0.002, 
	ground=True)
# create bird flock
x.kunfayakuna(
	number=200, 
	attraction_exponent=1, 
	repulsion_exponent=4,
	separation=0.01, 
	alignment_exponent=0.5, 
	alignment_factor=0.5,
	acceleration_factor=10,
	speed=0.5)
# run for 10 seconds
x.run(
	duration=10,
	cameratype='optimum')
# create predator
x.kunfayakuna(
	number=1, 
	attraction_exponent=1, 
	repulsion_exponent=4,
	separation=0.05, 
	alignment_exponent=0.5, 
	alignment_factor=1,
	acceleration_factor=5,
	predatorial_factor=1000,
	fearful_factor=5,
	speed=0.5,
	prey=0)
# run for 20 seconds
x.run(duration=20)
