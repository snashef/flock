import numpy as np
import time
import sys
import itertools
from vpython import *
from Flock import Flock

class Sky:
	def __init__(
		self,
		dimensions,
		interval,
		ground=False):
		"""
		dimensions: number of dimensions (2 or 3 please!)
		interval: size of smallest time unit (in seconds)
		duration: how long the sky lasts (in seconds)
		"""
		self.d = dimensions
		self.dt = interval
		self.ground = ground
		# empty list of bird flocks
		self.f = []
		# integer for every two second data
		self.integer = int(0.2 / self.dt)
		# colour dictionary
		self.col_list = [vec(1, 1, 0), vec(1, 0, 0), vec(0, 1, 0), vec(1, 0, 1)]
		# there is light
		self.there_is_light = False
		# camera type
		self.cameratype = None
		# current i
		self.current_i = 0
		# optimum observer camera data
		self.oo_data = {'pos': [], 'axis': [], 'up': []}

	def kunfayakuna(
		self, number, attraction_exponent, repulsion_exponent, 
		separation, alignment_exponent, alignment_factor, 
		acceleration_factor, speed, predatorial_factor=None, 
		fearful_factor=None, prey=None):
		"""
		incorporate bird flock
		number: number of birds in flock
		dimension: number of dimensions (2 or 3 please!)
		ground: z-value of ground (None if no ground impact)
		attraction_exponent, a: 1/r^a, exponent of attraction based on distance from neighbours, r
		repulsion_exponent, b: 1/r^b exponent of repulsion based on distance from neighbours, r
		separation: optimal distance of separation between neighbours
		alignment_exponent: c: 1/r^c exponent of velocity alignment based on distance from neighbours, r
		alignment_factor: coefficient of alignment factor wrt to separation
		acceleration_factor: coefficient of max acceleration (w.r.t. max speed)
		predatorial_factor: coefficient of predator's interest in prey (w.r.t. internal flock behaviour)
		fearful_factor: (for predators) coefficient of prey's fleeing from predator
		speed: speed birds fly
		"""
		colourindex = len(self.f)
		prey = self.f[prey]['math'] if prey is not None else None
		self.f.append({
			'math': Flock(
				number, self.d, attraction_exponent, repulsion_exponent, separation, 
				alignment_exponent, alignment_factor, acceleration_factor, predatorial_factor,
				fearful_factor, speed, colourindex, prey),
			'cone_length': separation,
			'cone_radius': separation / 5})

	def grid(self, grid_interval=0.4, grid_range=5.6):
		"""
		plot 2D or 3D grid
		grid_interval is gap between grid lines
		grid_range is width of square grid
		"""
		if self.ground:
			self.grid2D(grid_range, grid_interval)
		else:
			self.grid3D(grid_range, grid_interval * 2)

	def grid2D(self, grid_range, grid_interval):
		"""
		plot 2D grid
		"""
		gridquanta = (np.arange(int(grid_range / grid_interval))[1:] * grid_interval) - (grid_range / 2)
		xpos = [vector(-grid_range / 2, i, self.gridheight) for i in gridquanta]
		xaxis = vector(grid_range, 0, 0)
		ypos = [vector(i, -grid_range / 2, self.gridheight) for i in gridquanta]
		yaxis = vector(0, grid_range, 0)
		self.grid_x = [cylinder(pos=pos, axis=xaxis, radius=0.001, opacity=1) for pos in xpos]
		self.grid_y = [cylinder(pos=pos, axis=yaxis, radius=0.001, opacity=1) for pos in ypos]

	def grid3D(self, grid_range, grid_interval):
		"""
		plot 3D grid
		"""
		gridquanta = (np.arange(int(grid_range / grid_interval))[1:] * grid_interval) - (grid_range / 2)
		gridquanta = tuple(itertools.product(gridquanta, gridquanta))
		# xpos = [vector(-grid_range / 2, i, j) for i, j in gridquanta]
		xpos = [vector(0, i, j) for i, j in gridquanta]
		xaxis = vector(grid_range, 0, 0)
		# ypos = [vector(i, -grid_range / 2, j) for i, j in gridquanta]
		ypos = [vector(i, 0, j) for i, j in gridquanta]
		yaxis = vector(0, grid_range, 0)
		# zpos = [vector(i, j, -grid_range / 2) for i, j in gridquanta]
		zpos = [vector(i, j, 0) for i, j in gridquanta]
		zaxis = vector(0, 0, grid_range)
		# self.grid_x = [cylinder(pos=pos, axis=xaxis, radius=0.005, opacity=0.1) for pos in xpos]
		# self.grid_y = [cylinder(pos=pos, axis=yaxis, radius=0.005, opacity=0.1) for pos in ypos]
		# self.grid_z = [cylinder(pos=pos, axis=zaxis, radius=0.005, opacity=0.1) for pos in zpos]
		self.grid_x = [box(pos=pos, axis=xaxis, height=0.005, width=0.005, opacity=0.1) for pos in xpos]
		self.grid_y = [box(pos=pos, axis=yaxis, height=0.005, width=0.005, opacity=0.1) for pos in ypos]
		self.grid_z = [box(pos=pos, axis=zaxis, height=0.005, width=0.005, opacity=0.1) for pos in zpos]

	def lettherebelight(self, cameratype):
		"""
		plots space
		"""
		# plots 3d space
		self.canvas = canvas(title="flock", width=1000, height=600, autoscale=False)
		# set grid height
		if len(self.f) > 0:
			self.gridheight = -self.f[0]['math'].sep * self.f[0]['math'].ts * 20
		else:
			self.gridheight = -0.1
		# position camera
		self.setcamera(cameratype)	
		# plots grid
		self.grid()	

	def proceedintime(self, duration, cameratype, counter, forces, positions, camera):
		# delete all non-curve objects (i.e. not grid) from canvas
		for canvas_object in self.canvas.objects:
			if canvas_object._objName == 'cone':
				canvas_object.visible = False
				del canvas_object
		# create cones list
		for f in self.f:
			f['cones'] = [cone(
				pos=vector(bird[0], bird[1], bird[2]),
				size=vector(f['cone_length'], f['cone_radius'] * 0.4, f['cone_radius'] * 1.5),
				axis=vector(bird[3], bird[4], bird[5]),
				color=self.col_list[int(bird[6])]
			) for bird in f['math'].vpf]
		# update cone co-ordinates in time
		for i in range(int(duration / self.dt)):
			# print data to terminal
			self.terminal_data(counter, forces, positions, camera, i)
			time.sleep(self.dt)
			for f in self.f:
				f['math'].shift(self.dt)
				for Cone in enumerate(f['cones']):	
					Cone[1].pos = vector(f['math'].vpf[Cone[0]][0], f['math'].vpf[Cone[0]][1], f['math'].vpf[Cone[0]][2])
					Cone[1].axis = vector(f['math'].vpf[Cone[0]][3], f['math'].vpf[Cone[0]][4], f['math'].vpf[Cone[0]][5])
					Cone[1].size = vector(f['cone_length'], f['cone_radius'] * 0.4, f['cone_radius'] * 1.5)
			self.setcamera(cameratype, i)
			# print(np.mean(self.f[0]['math'].p[2, :]), np.std(self.f[0]['math'].p[2, :]))
		self.current_i += int(duration / self.dt)

	def terminal_data(self, counter, forces, positions, camera, i):
		"""
		prints counter, positions and forces data to terminal 
		(positions and forces data of first flock)
		"""
		CURSER_UP_ONE = '\x1b[1A'
		ERASE_LINE = '\x1b[2K'
		string = ''
		if counter:
			# every 0.01 seconds
			if i % int(0.01 / self.dt) == 0:
				self.counter_string = '\rtime: %1.2fs\n' % ((i + self.current_i) * self.dt)
			string += self.counter_string
			sys.stdout.write(CURSER_UP_ONE + ERASE_LINE)
		if camera:
			# every 0.1 seconds
			if i % int(0.1 / self.dt) == 0:
				pos = '%1.2f,%1.2f,%1.2f' % (self.canvas.camera.pos.x, self.canvas.camera.pos.y, self.canvas.camera.pos.z)
				axis = '%1.2f,%1.2f,%1.2f' % (self.canvas.camera.axis.x, self.canvas.camera.axis.y, self.canvas.camera.axis.z)
				up = '%1.2f,%1.2f,%1.2f' % (self.canvas.up.x, self.canvas.up.y, self.canvas.up.z)
				self.camera_string = 'camera pos: %s    axis: %s    up: %s\n' % (pos, axis, up)
			string += self.camera_string
			sys.stdout.write(CURSER_UP_ONE + ERASE_LINE)
		if positions:
			# every 0.1 seconds
			if i % int(0.1 / self.dt) == 0:
				centre_of_mass = '%1.2f,%1.2f,%1.2f' % (tuple([j for j in self.f[0]['math'].centre_of_mass()]))
				self.positions_string = 'com: %s    median distance from com: %1.2f\n' % (
					centre_of_mass, self.f[0]['math'].median_distance())
			string += self.positions_string
			sys.stdout.write(CURSER_UP_ONE + ERASE_LINE)
		if forces:
			# every 0.1 seconds
			if i % int(0.1 / self.dt) == 0:
				self.forces_string = '   '.join(['%s: %1.2f' % (j[0], np.sum(np.sum(j[1] ** 2, axis=1)) ** 0.5) for j in self.f[0]['math'].da.items()])
			string += self.forces_string
		sys.stdout.write(string + '\r')
		sys.stdout.flush()

	def setcamera(self, cameratype, i=0):
		"""
		sets camera position
		"""
		# vis_data = self.f[0].com_and_median_distance()
		# b = np.mean(np.abs(vis_data[1])) * 2
		# h = b / np.tan(np.pi / 6)
		# self.canvas.center = vector(0, 0, 0)
		if cameratype == 'stationary':
			pos, axis, up = self.stationary_observer(-0.2, -0.2, self.gridheight + 0.05)
			# pos, axis, up = self.stationary_observer(-0.5, -0.5, self.gridheight + 0.05)
		elif cameratype == 'orbiting':
			pos, axis, up = self.orbiting_observer(0.3, self.gridheight + 0.05, i)
		elif cameratype == 'bird':
			pos, axis, up = self.bird_observer(0)
		elif cameratype == 'optimum':
			pos, axis, up = self.optimum_observer(i)
		self.canvas.camera.pos = vector(pos[0], pos[1], pos[2])
		self.canvas.camera.axis = vector(axis[0], axis[1], axis[2])
		self.canvas.up = vector(up[0], up[1], up[2])

	def flat_xy_up(self, axis):
		# calculate up based on axis and a horizontal x-y plane
		k = np.array([
			axis[0] * np.cos(-np.pi / 2) - axis[1] * np.sin(-np.pi / 2), 
			axis[0] * np.sin(-np.pi / 2) + axis[1] * np.cos(-np.pi / 2),
			0])
		k = k / (np.sum(k ** 2) ** 0.5)
		return np.cos(np.pi / 2) + np.cross(k, axis) * np.sin(np.pi / 2) + k * (np.dot(k, axis)) * (1 - np.cos(np.pi / 2))

	def stationary_observer(self, x, y, z, comflockno=0):
		"""
		returns pos and axis for stationary observer fixing gaze on centre of mass
		x, y, z are x, y, z co-ordinates of observer position
		"""
		# get centre of mass
		com = self.f[comflockno]['math'].centre_of_mass()
		pos = np.array([x, y, z])
		axis = com - pos
		up = self.flat_xy_up(axis)
		return pos, axis, up

	def optimum_observer(self, i, comflockno=0):
		"""
		returns pos and axis for optimum observer fixing gaze on centre of mass and following flock
		remaining on the y axis
		"""
		# get flock position data
		com = self.f[comflockno]['math'].centre_of_mass()
		median = self.f[comflockno]['math'].median_distance(com)
		# x position at a suitable distance from flock depending on spread
		x = com[0] + 2 * (3 ** 0.5) * median
		# z position just above ground or at com of flock
		if self.ground:
			z = self.gridheight + 0.05
		else:
			z = com[2]
			# self.grid()
		# append camera data to list
		self.oo_data['pos'].append(np.array([x, com[1], z]))
		self.oo_data['axis'].append(com - self.oo_data['pos'][-1])
		self.oo_data['up'].append(self.flat_xy_up(self.oo_data['axis'][-1]))
		# take average of previous second's optimum camera data
		smoothingperiod = 1
		meaninterval = int(smoothingperiod / self.dt)
		if len(self.oo_data['pos']) > meaninterval:
			for datum in ['pos', 'axis', 'up']:
				self.oo_data[datum] = self.oo_data[datum][-meaninterval:]
		return np.mean(self.oo_data['pos'], axis=0), np.mean(self.oo_data['axis'], axis=0), np.mean(self.oo_data['up'], axis=0)

	def orbiting_observer(self, r, z, i, f=0.1, comflockno=0):
		"""
		returns pos and axis for orbiting observer fixing gaze on centre of mass
		r is distance from centre of mass
		z is height
		f is frequency of complete orbit (per second)
		"""
		com = self.f[comflockno]['math'].centre_of_mass()
		phi = 2 * np.pi * f * self.dt * (i + self.current_i)
		dz = com[2] - z
		dr = (r ** 2 - dz ** 2) ** 0.5
		x = dr * np.sin(phi)
		y = dr * np.cos(phi)
		pos = np.array([x + com[0], y + com[1], z])
		axis = com - pos
		up = self.flat_xy_up(axis)
		return pos, axis, up

	def bird_observer(self, bird, birdflockno=0):
		"""
		returns pos and axis for bird
		bird is the number bird to follow
		"""
		axis = self.f[birdflockno]['math'].v[:, 0]
		pos = self.f[birdflockno]['math'].p[:, 0] + 2 * (axis * self.f[birdflockno]['cone_length'])
		# up = self.f[0].a[:, 0]
		up = self.flat_xy_up(axis)
		return pos, axis, up

	def get_data(self, flocks, i):
		"""
		get update of bird flock data every 0.2 seconds
		"""
		if i % self.integer == 0:
			data = [(j[0], j[1]) for j in [f.data() for f in flocks]]
			stringlist = ['\rflock %d; ratio: %1.2f; com: x=%1.2f, y=%1.2f, z=%1.2f; time=%1.2fs\r' % (
				d[0] + 1, d[1][0], d[1][1][0], d[1][1][1], d[1][1][2], i * self.dt) for d in enumerate(data)]
			sys.stdout.write(''.join(stringlist))
			sys.stdout.flush()

	def run(self, duration, cameratype=None, counter=True, forces=True, positions=True, camera=True):
		"""
		runs simulation
		duration is duration of simulation
		cameratype: ["orbiting", "stationary", "bird"] (default = stationary)
		"""
		if cameratype is None:
			if self.cameratype is None:
				cameratype, self.cameratype = 'stationary', 'stationary'
			else:
				cameratype = self.cameratype
		else:
			self.cameratype = cameratype
		if self.there_is_light is False:
			self.lettherebelight(cameratype)
			self.there_is_light = True
		self.proceedintime(duration, cameratype, counter, forces, positions, camera)
