import numpy as np

class Flock:
	def __init__(
		self,
		number,
		dimensions,
		attraction_exponent,
		repulsion_exponent,
		separation,
		alignment_exponent,
		alignment_factor,
		acceleration_factor,
		predatorial_factor,
		fearful_factor,
		speed,
		colour,
		prey):
		"""
		number: number of birds in flock
		dimension: number of dimensions (2 or 3 please!)
		ground: z-value of ground (None if no ground impact)
		attraction_exponent, a: 1/r^a, exponent of attraction based on distance from neighbours, r
		repulsion_exponent, b: 1/r^b exponent of repulsion based on distance from neighbours, r
		separation: optimal distance of separation between neighbours
		alignment_exponent: c: 1/r^c exponent of velocity alignment based on distance from neighbours, r
		alignment_factor: coefficient of alignment factor wrt to separation
		acceleration_factor: coefficient of max acceleration (w.r.t. max speed)
		predatorial_factor: coefficient of predator's interest in prey (w.r.t. internal flock behaviour)
		fearful_factor: (for predators) coefficient of prey's fleeing from predator
		speed: speed birds fly
		"""
		self.n = number
		self.re = repulsion_exponent
		self.af = alignment_factor
		self.ae = attraction_exponent
		self.ale = alignment_exponent
		self.acf = acceleration_factor
		self.pred_f = predatorial_factor
		self.fearful_factor = fearful_factor
		self.d = dimensions
		self.c = colour
		self.ts = speed
		self.sep = separation
		# set repulsion factor
		self.rf = separation ** (self.re - self.ae)
		# normalise alignment factor
		multiple = (separation ** (self.ale - self.ae)) / self.ts
		self.af *= multiple
		# initialise flock
		self.p = np.random.random((self.d, self.n)) - 0.5
		self.v = np.random.random((self.d, self.n)) - 0.5
		self.a = np.zeros((self.d, self.n))
		# limit speed of each bird
		self.limit_speed()
		# predator and prey list
		self.predators = []
		self.prey = prey
		if prey:
			self.inspire_fear()
			self.choose_victim()
		# acceleration data dictionary
		self.da = {}
		# make vpython friendly data
		self.vpythonfriendly()
		# identical starting velocity
		# self.v = np.stack([self.v[:, 0] for i in range(self.n)], axis=1)

	def inspire_fear(self):
		"""
		inspire fear in prey
		"""
		self.prey.fear(self)

	def fear(self, predator):
		"""
		fear predator
		"""
		self.predators.append(predator)
		self.ff = predator.fearful_factor

	def choose_victim(self):
		"""
		if predator, choose random victim from prey flock
		"""
		self.victims = np.random.randint(low=0, high=self.prey.n, size=self.n)

	def external_predator_data(self):
		"""
		get matrices of distance from predators
		"""
		if self.predators:
			# matrix of predator positions
			p_p = np.hstack([i.p for i in self.predators])
			# matrix of displacements of each predator from each bird
			self.pred_r = np.array([np.subtract.outer(self.p[i], p_p[i]) for i in range(3)])
			# matrix of r^2
			pred_r2 = self.pred_r ** 2
			pred_r2 = np.sum(pred_r2, axis=0)
			# matrix of 1/r^2
			self.pred_invr2 = pred_r2 ** -1
			# matrix of unit vector directions to predators
			self.pred_uvec = self.pred_invr2 * self.pred_r

	def internal_flock_data(self):
		"""
		get matrices of distances between all birds
		"""
		# matrix of displacements
		self.r = np.array([np.subtract.outer(i, i) for i in self.p])
		# matrix of r^2 (distance)
		r2 = self.r ** 2
		r2 = np.sum(r2, axis=0)
		# matrix of 1/r^2
		r2 += 1000 * np.identity(self.n)
		self.invr2 = r2 ** -1	
		# matrix of unit vector directions to nearest birds
		self.uvec = self.invr2 * self.r
		# matrix of neighbours' velocities
		self.unv = np.stack([self.v for i in range(self.n)], axis=1) / self.ts

	def shift_toward_prey(self, dt):
		"""
		accelerates toward random bird from prey flock based on pred_f
		where pred_f is self.pred_f (predatorial factor)
		NOTE: no normalisation here, just (non-unit direction * pred_f) however
		should be fine because of normalisation elsewhere
		"""
		if self.prey:
			self.da['prey'] = (self.prey.p[:, self.victims] - self.p) * self.pred_f
			self.a += self.da['prey']

	def shift_away_from_predator(self, dt):
		"""
		accelerates away from predators based on c * 1/r^2
		where c is self.ff (fear factor)
		"""
		if self.predators:
			self.da['predator'] = np.sum(self.pred_invr2 * self.pred_uvec, axis=2) * dt * self.ff
			self.a += self.da['predator']

	def shift_toward_nearest_birds(self, dt):
		"""
		accelerates towards nearest birds based on a factor of 1/r^n
		where n is self.ae (attraction exponent)
		and r is distance from that bird
		"""
		self.da['attraction'] = np.sum((self.invr2 ** (self.ae * 0.5)) * self.uvec, axis=1) * dt
		self.a += self.da['attraction']

	def shift_away_from_nearest_birds(self, dt):
		"""
		accelerates away from nearest birds based on a factor of c * 1/r^n
		where n is self.re (repulsion exponent) and c is self.rf (repulsion factor)
		and r is distance from that bird
		"""
		self.da['repulsion'] = -np.sum((self.invr2 ** (self.re * 0.5)) * self.uvec, axis=1) * dt * self.rf
		self.a += self.da['repulsion']

	def align_velocity_with_nearest_birds(self, dt):
		"""
		accelerates in line with velocity of nearest birds based on a factor of c * 1/r^n
		where n is self.ae (alignment exponent) and c is self.af (alignment factor)
		and r is distance from that bird
		"""
		self.da['alignment'] = np.sum(self.invr2 ** (self.ale * 0.5) * self.unv, axis=1) * dt * self.af
		self.a += self.da['alignment']

	def limit_acceleration(self, dt):
		"""
		limits acceleration magnitude to f times the top speed
		where f is factor
		"""
		maxshiftmagnitude = self.ts * dt * self.acf
		magshift = np.sum(self.a ** 2, axis=0) ** 0.5
		magshift = np.where(magshift < maxshiftmagnitude, maxshiftmagnitude, magshift)
		self.a *= maxshiftmagnitude * magshift ** -1

	def limit_speed(self):
		"""
		limits magnitude of velocity to self.ts
		"""
		self.v *= self.ts * np.sum(self.v ** 2, axis=0) ** -0.5

	def velocityshift(self, dt):
		"""
		shift velocity based on attraction, repulsion, alignment and predators
		"""
		# calculate separation data
		self.internal_flock_data()
		self.external_predator_data()
		# calculate acceleration
		self.a = np.zeros((self.d, self.n))
		self.shift_toward_nearest_birds(dt)
		self.shift_away_from_nearest_birds(dt)
		self.align_velocity_with_nearest_birds(dt)
		self.shift_toward_prey(dt)
		self.shift_away_from_predator(dt)
		# limit acceleration magnitude
		self.limit_acceleration(dt)
		# shift velocity
		self.v += self.a
		# limit speed
		self.limit_speed()
		# predator shift
		# if len(self.predators) > 0:
		# 	# get predator data
		# 	self.predator_data()

	def positionshift(self, dt):
		"""
		shift position based on current velocity
		"""
		self.p += self.v * dt

	def shift(self, dt):
		"""
		general position-velocity shift
		"""
		self.positionshift(dt)
		self.velocityshift(dt)
		self.vpythonfriendly()

	def vpythonfriendly(self):
		"""
		update vpython friendly data
		"""
		self.vpf = np.append(np.swapaxes(self.p, 0, 1), np.swapaxes(self.v, 0, 1), axis=1)
		colors = np.array([[self.c] for i in range(self.n)])
		self.vpf = np.append(self.vpf, colors, axis=1)

	def data(self):
		"""
		returns data on flock at current moment in time:
		ratio of shifts
		centre of mass
		"""
		# check ratio of shifts
		gvs_mag = np.mean(np.sum(self.avs ** 2, axis=0) ** 0.5)
		svs_mag = np.mean(np.sum(self.rvs ** 2, axis=0) ** 0.5)
		avs_mag = np.mean(np.sum(self.alvs ** 2, axis=0) ** 0.5)
		ratio = np.mean([gvs_mag, svs_mag]) / avs_mag
		# check centre of mass
		com = np.mean(self.p, axis=1)
		return ratio, com

	def median_distance(self, com=None):
		"""
		returns median distance from centre of mass
		"""
		if com is None:
			com = np.mean(self.p, axis=1)
		vector_med = np.median(np.abs(self.p - np.array([com]).swapaxes(0, 1)), axis=1)		
		return np.sum(vector_med ** 2) ** 0.5

	def centre_of_mass(self):
		"""
		returns centre of mass
		"""
		return np.mean(self.p, axis=1)










