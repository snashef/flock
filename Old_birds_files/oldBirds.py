import numpy as np
import time
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

class Flock:
	def __init__(
		self,
		number,
		size,
		robswivelfactor,
		comswivelfactor,
		nearswivelfactor,
		velocityfactor,
		name,
		color=None
	):
		"""
		number: number of birds in flock
		size: size of space they originally occupy
		comswivelfactor: rate of swivel toward centre of mass
		robswivelfactor: rate of swivel toward fixed random bird
		nearswivelfactor: rate of swivel toward nearest bird
		velocityfactor: general magnitude of velocities wrt size of flock
		e.g. birds = Flock(
			number=20, 
			size=5, 
			robswivelfactor=0, 
			comswivelfactor=0.1, 
			nearswivelfactor=1, 
			velocityfactor=0.5
		)
		"""
		self.n = number
		self.s = size
		self.p = np.zeros((3, self.n))
		self.v = np.zeros((3, self.n))
		self.rsw = robswivelfactor
		self.csw = comswivelfactor
		self.nsw = nearswivelfactor
		self.vf = velocityfactor
		self.rm = np.random.randint(0, self.n, self.n)
		self.name = name
		self.c = color

	def init_flock(self):
		"""
		randomly initialises bird flock positions and velocities
		"""
		self.p = (np.random.random((3, self.n)) * self.s) - (self.s / 2)
		self.v = (np.random.random((3, self.n)) * self.s) - (self.s / 2)

	def move(self, dt):
		"""
		time-shift position
		"""
		self.p += self.v * dt * self.vf

	def com(self, x):
		"""
		returns centre of mass of position array x
		"""
		return np.array([(sum(i) / self.n) for i in x])

	def comswivel(self):
		"""
		time-shift velocity toward centre of mass
		"""
		com = self.com(self.p)
		tendto = np.swapaxes(np.stack([com] * self.n), 0, 1)
		self.v += (tendto - self.p) * self.csw

	def robswivel(self):
		"""
		time-shift velocity toward fixed random other bird
		"""
		tendto = self.p[:, self.rm]
		self.v += (tendto - self.p) * self.rsw

	def nearswivel(self):
		"""
		time-shift velocity toward nearest bird
		"""
		tendto = self.p[:, self.findnearest(self.p)]
		self.v += (tendto - self.p) * self.nsw

	def findnearest(self, x):
		"""
		return mapping matrix of nearest bird
		"""
		diffs = np.array([np.subtract.outer(i, i) for i in x]) ** 2
		diffs = np.add(diffs[0], np.add(diffs[1], diffs[2]))
		# add 1000 * identity matrix to ignore self in finding nearest bird
		diffs += self.s * 1000 * np.identity(self.n)
		diffs = diffs.argmin(axis=1)
		return diffs

	def obsswivel(self, x, s):
		"""
		velocity-shift away from obstacle
		"""
		# distance from obstacle
		dxdy = np.subtract(self.p[:2], np.reshape(x, (2, 1)))
		dr = dxdy ** 2
		dr = np.add(dr[0], dr[1]) - (s ** 2)
		# swerve factor and direction
		swerve = 1 / dr
		self.v += np.append(dxdy, np.zeros((1, self.n))).reshape(3, self.n) * swerve

	def shift(self, dt, obstacle):
		"""
		total time-shift of position and velocity
		"""
		self.move(dt)
		self.comswivel()
		self.robswivel()
		self.nearswivel()
		for i in obstacle:
			self.obsswivel(i[:2], i[2])

class Sky:
	def __init__(
		self,
		interval,
		size,
		duration
	):
		"""
		interval: size of smallest time unit (in seconds)
		size: size of dimensions
		duration: number of steps animation runs for
		e.g. sky = Sky(
			interval=0.01,
			size=7,
			duration = 500
		)
		"""
		self.dt = interval
		self.s = size
		self.N = duration
		self.b = []  # empty set to be filled with flock instances
		self.n = 0  # number of flocks of birds in sky
		self.o = []  # empty set to be filled with obstacle instances

	def kunfayakunu(self, x):
		"""
		incorporate Flock instance (or list of instances) into Sky
		e.g. sky.kunfayakunu('pigeon')
		"""
		if not isinstance(x, list):
			print('ERROR: must call beings into existence as list')
		for i in x:
			if isinstance(i, Flock):
				# acknowledge
				self.b.append(i)
				# initialise
				i.init_flock()
				# count
				self.n += 1

	def tree(self, size, position=None, visible=False):
		"""
		create an obstacle in the sky
		"""
		if not position:
			position = np.random.random((2, 1)) * self.s - (self.s / 2)
		self.o.append(np.append(position, np.append(size, visible)))

	def plot(self, axis=False, cube=True, xyz=True, autorotate=True, legend=True):
		"""
		plots space
		"""
		# set up space
		fig = plt.figure(figsize=(8, 8))
		ax = fig.add_subplot(111, projection='3d')
		ax.set_xlim(-self.s * 1.5, self.s * 1.5)
		ax.set_ylim(-self.s * 1.5, self.s * 1.5)
		ax.set_zlim(-self.s * 1.5, self.s * 1.5)
		ax.grid(False)
		# visual aids
		for i in self.o:
			if i[3] == 1:
				self.vis_tree(ax, i[:3])
		if axis is False:
			ax.set_axis_off()
		if cube:
			self.cube(ax)
		if xyz:
			self.xyz(ax)
		# plot
		b_scatter = [None for j in range(self.n)]
		for i in range(self.N):
			# remove previous plot
			for j in b_scatter:
				if j:
					ax.collections.remove(j)
			# plot new plot
			for j in range(self.n):
				x = self.b[j].p
				color = self.b[j].c
				color = color if color else 'C%d' % j
				name = self.b[j].name
				b_scatter[j] = ax.scatter(
					x[0], x[1], x[2], 
					marker='o', 
					color=color,
					label=name)
			# legend
			if legend:
				ax.legend(loc=1)
			# pause
			plt.pause(self.dt)
			# rotate axes
			if autorotate:
				self.rotateaxes(ax, i)
			# shift bird flock
			for j in range(self.n):
				self.b[j].shift(self.dt, self.o)

	def cube(self, ax):
		"""
		plots transparent cube
		"""
		# cube vertices
		a = self.s
		x = [[a * (-1) ** (i // j) for j in (1, 2, 4)] for i in range(8)]
		# cube sides
		s = [
			[x[i] for i in (0, 1, 3, 2)],
			[x[i] for i in (4, 5, 7, 6)],
			[x[i] for i in (0, 2, 6, 4)],
			[x[i] for i in (1, 3, 7, 5)],
			[x[i] for i in (0, 1, 5, 4)],
			[x[i] for i in (2, 3, 7, 6)]
		]
		collection = Poly3DCollection(
			s, edgecolors='black', linewidths=0.5, alpha=0.2)
		collection.set_facecolor([1, 1, 1])
		ax.add_collection3d(collection)

	def vis_tree(self, ax, obs):
		"""
		plots tree in its position
		"""
		# tree height
		h = self.s * 3
		# circle in xy coordinates
		xy = [[np.sin(i), np.cos(i), np.sin(i - 1), np.cos(i - 1)] for i in np.arange(0, 2 * np.pi, np.pi / 36)]
		# translate to tree position
		xy = np.multiply(xy, obs[2]) + np.append(obs[:2], obs[:2])
		# cylinder sides
		s = [[
			[i[0], i[1], -h], 
			[i[2], i[3], -h], 
			[i[2], i[3], h],
			[i[0], i[1], h]] for i in xy]
		# plot
		collection = Poly3DCollection(
			s, edgecolors=None, linewidth=0.5)
		ax.add_collection3d(collection)

	def xyz(self, ax):
		"""
		plots off-centre small x-y-z axes
		"""
		a = 1.5 * self.s
		b = a * 1.3
		c = a * 1.4
		x = [a, b, a, a, a, a]
		y = [a, a, a, b, a, a]
		z = [a, a, a, a, a, b]
		ax.plot(x, y, z, color='black')
		ax.text(c, a, a, 'x', color='gray')
		ax.text(a, c, a, 'y', color='gray')
		ax.text(a, a, c, 'z', color='gray')

	def rotateaxes(self, ax, step):
		"""
		rotates axes
		"""
		step = step * 0.005
		x = (180 * np.sin(step))
		y = (180 * np.cos(step))
		ax.view_init(x, y)






