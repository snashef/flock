from birds import Flock

a = Flock(
	number=20, 
	size=10, 
	interval=0.01, 
	comswivelfactor=0.1, 
	robswivelfactor=0,
	nearswivelfactor=1,
	velocityfactor=0.5,
	steps=1000
)

for i in range(6):
	a.init_flock()

a.plot(
	autorotate=True,
	square=True,
	xyz=True,
	axis=False,
	rob=False, 
	com=True,
	commoving=False,
	nea=True
)

