import numpy as np
import time
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import sys
from vpython import *

class Flock:
	def __init__(
		self,
		number,
		dimensions,
		attraction_exponent,
		repulsion_exponent,
		separation,
		alignment_exponent,
		alignment_factor,
		speed,
		colour):
		"""
		number: number of birds in flock
		dimension: number of dimensions (2 or 3 please!)
		ground: z-value of ground (None if no ground impact)
		attraction_exponent, a: 1/r^a, exponent of attraction based on distance from neighbours, r
		repulsion_exponent, b: 1/r^b exponent of repulsion based on distance from neighbours, r
		separation: optimal distance of separation between neighbours
		alignment_exponent: c: 1/r^c exponent of velocity alignment based on distance from neighbours, r
		alignment_factor: coefficient of alignment factor wrt to separation
		speed: speed birds fly
		"""
		self.n = number
		self.re = repulsion_exponent
		self.af = alignment_factor
		self.ae = attraction_exponent
		self.ale = alignment_exponent
		self.d = dimensions
		self.c = colour
		self.ts = speed
		self.sep = separation
		# set repulsion factor
		self.rf = separation ** (self.re - self.ae)
		# normalise alignment factor
		multiple = separation ** (self.ale - self.ae)		
		self.af *= multiple
		# initialise flock
		self.p = np.random.random((self.d, self.n)) - 0.5
		self.v = np.random.random((self.d, self.n)) - 0.5
		self.a = np.random.random((self.d, self.n)) - 0.5
		# limit speed of each bird
		self.v *= self.ts * np.sum(self.v ** 2, axis=0) ** -0.5
		# predator list
		self.predators = []
		# make vpython friendly data
		self.vpythonfriendly()
		# identical starting velocity
		# self.v = np.stack([self.v[:, 0] for i in range(self.n)], axis=1)

	def positionshift(self, dt):
		"""
		shift position based on current velocity
		"""
		self.p += self.v * dt

	def internal_separation_data(self):
		"""
		get matrices of distances between all birds
		"""
		# matrix of displacements
		self.r = np.array([np.subtract.outer(i, i) for i in self.p])
		# matrix of r^2 (distance)
		r2 = self.r ** 2
		r2 = np.sum(r2, axis=0)
		# matrix of 1/r^2
		r2 += 1000 * np.identity(self.n)
		self.invr2 = r2 ** -1
		# self.invr2 = np.where(r2 == 0, 0, r2 ** -1)		
		# matrix of unit vector directions to nearest birds
		self.uvec = self.invr2 * self.r
		# matrix of neighbours' velocities
		self.unv = np.stack([self.v for i in range(self.n)], axis=1) / self.ts

	def predator_data(self):
		"""
		get matrices of distance from predator
		"""
		# matrix of displacements
		p_p = np.hstack([i.p for i in self.predators])
		import pdb; pdb.set_trace()  # breakpoint 00c5db4b //
		self.pred_r = np.subtract.outer(self.p, p_p)

	def fear(self, predator):
		"""
		incorporate predator data into flock
		"""
		self.predators.append(predator)

	def velocityshift(self, dt):
		"""
		shift velocity based on:
		glue factor (tend to nearest neighbour based on r^-2)
		separation factor (tend away from nearest neighbour based on r^-6)
		velocity factor (tend to nearest neighbour's velocity)
		"""
		# get separation data
		self.internal_separation_data()
		# tend to nearest birds (based on 1/r^a)
		self.avs = np.sum((self.invr2 ** (self.ae * 0.5)) * self.uvec, axis=1) * dt
		# separation shift (based on 1/r^b)
		self.rvs = -np.sum((self.invr2 ** (self.re * 0.5)) * self.uvec, axis=1) * dt * self.rf
		# alignment shift (based on 1/r)
		self.alvs = np.sum(self.invr2 ** (self.ale * 0.5) * self.unv, axis=1) * dt * self.af
		# predator shift
		if len(self.predators) > 0:
			# get predator data
			self.predator_data()
		# limit shift (10 * greater than speed limit) then shift
		self.a = self.avs + self.rvs + self.alvs
		maxshiftmagnitude = self.ts * dt * 10
		magshift = np.sum(self.a ** 2, axis=0) ** 0.5
		magshift = np.where(magshift < maxshiftmagnitude, maxshiftmagnitude, magshift)
		self.a *= maxshiftmagnitude * magshift ** -1
		self.v += self.a
		# limit speed of each bird
		speedsquared = np.sum(self.v ** 2, axis=0)
		self.v *= self.ts * speedsquared ** -0.5

	def shift(self, dt):
		"""
		general position-velocity shift
		"""
		self.positionshift(dt)
		self.velocityshift(dt)
		self.vpythonfriendly()

	def vpythonfriendly(self):
		"""
		update vpython friendly data
		"""
		self.vpf = np.append(np.swapaxes(self.p, 0, 1), np.swapaxes(self.v, 0, 1), axis=1)
		colors = np.array([[self.c] for i in range(self.n)])
		self.vpf = np.append(self.vpf, colors, axis=1)

	def data(self):
		"""
		returns data on flock at current moment in time:
		ratio of shifts
		centre of mass
		"""
		# check ratio of shifts
		gvs_mag = np.mean(np.sum(self.avs ** 2, axis=0) ** 0.5)
		svs_mag = np.mean(np.sum(self.rvs ** 2, axis=0) ** 0.5)
		avs_mag = np.mean(np.sum(self.alvs ** 2, axis=0) ** 0.5)
		ratio = np.mean([gvs_mag, svs_mag]) / avs_mag
		# check centre of mass
		com = np.mean(self.p, axis=1)
		return ratio, com

	def median_distance(self):
		"""
		returns median distance from centre of mass
		"""
		com = np.mean(self.p, axis=1)
		return np.median(np.abs(self.p - np.array([com]).swapaxes(0, 1)), axis=1)

	def centre_of_mass(self):
		"""
		returns centre of mass
		"""
		return np.mean(self.p, axis=1)

class Predator:
	def __init__(
		self,
		dimensions,
		number,
		distance,
		agility_factor,
		prey,
		speed,
		colour):
		"""
		"""
		self.d = dimensions
		self.n = number
		# get position from radial distance from origin
		self.p = np.random.random((self.d, self.n)) - 0.5
		self.p = (self.p / (np.sum(self.p ** 2, axis=0) ** 0.5)) * distance
		self.prey = prey
		# self.v = np.zeros((3, self.n))
		self.v = np.random.random((self.d, self.n)) - 0.5
		self.ts = speed
		self.c = colour
		self.af = agility_factor
		self.vpythonfriendly()
		self.inspire_fear()

	def positionshift(self, dt):
		"""
		adjusts position based on velocity
		"""
		self.p += self.v * dt

	def velocityshift(self, dt):
		"""
		adjusts velocity based on position of prey
		"""		
		# calculate a, acceleration of predator
		self.a = (self.prey.p[:, 0].reshape(self.d, 1) - self.p)
		# limit acceleration to 10x top speed
		max_a = self.ts * dt * 10
		magshift = np.sum(self.a ** 2, axis=0) ** 0.5
		magshift = np.where(magshift < max_a, max_a, magshift)		
		self.a *= max_a * magshift ** -1
		# shift velocity
		self.v += self.a * dt * self.af
		# normalise velocity
		speedsquared = np.sum(self.v ** 2, axis=0)
		self.v *= self.ts * speedsquared ** -0.5

	def shift(self, dt):
		"""
		general position-velocity shift
		"""
		self.positionshift(dt)
		self.velocityshift(dt)
		self.vpythonfriendly()

	def vpythonfriendly(self):
		"""
		outputs vpython friendly data
		"""
		self.vpf = np.append(np.swapaxes(self.p, 0, 1), np.swapaxes(self.v, 0, 1), axis=1)
		colors = np.array([[self.c] for i in range(self.n)])
		self.vpf = np.append(self.vpf, colors, axis=1)

	def inspire_fear(self):
		"""
		inspire fear in prey
		"""
		self.prey.fear(self)

class Sky:
	def __init__(
		self,
		dimensions,
		interval):
		"""
		dimensions: number of dimensions (2 or 3 please!)
		interval: size of smallest time unit (in seconds)
		duration: how long the sky lasts (in seconds)
		"""
		self.d = dimensions
		self.dt = interval
		# empty list of bird flocks
		self.f = []
		# integer for every two second data
		self.integer = int(0.2 / self.dt)
		# colour dictionary
		self.col_list = [vec(1, 1, 0), vec(1, 0, 0), vec(0, 1, 0), vec(1, 0, 1)]
		# there is light
		self.there_is_light = False
		# camera type
		self.cameratype = None
		# current i
		self.current_i = 0

	def kunfayakuna(self, number, attraction_exponent, repulsion_exponent, separation, alignment_exponent, alignment_factor, speed):
		"""
		incorporate bird flock
		number: number of birds in flock
		attraction_exponent, a: 1/r^a, exponent of attraction based on distance from neighbours, r
		repulsion_exponent, b: 1/r^b exponent of repulsion based on distance from neighbours, r
		separation: optimal distance of separation between neighbours
		alignment_exponent: c: 1/r^c exponent of velocity alignment based on distance from neighbours, r
		alignment_factor: coefficient of alignment factor wrt to separation
		speed: speed birds fly
		ground: z-value of ground (None of no ground)
		"""
		colourindex = len(self.f)
		self.f.append({
			'math': Flock(
				number, self.d, attraction_exponent, repulsion_exponent, separation, 
				alignment_exponent, alignment_factor, speed, colourindex),
			'cone_length': separation,
			'cone_radius': separation / 5})

	def kunfapredator(self, number, distance, prey, speed, agility_factor):
		"""
		incorporate a predator
		number: number of predators
		position: starting position of predators (np.array)
		prey: prey (insert index of flock to be pursued as integer)
		speed: top speed
		"""
		colourindex = len(self.f)
		self.f.append({
			'math': Predator(
				self.d, number, distance, agility_factor, self.f[prey]['math'], speed, colourindex),
			'cone_length': self.f[prey]['math'].sep * 5,
			'cone_radius': self.f[prey]['math'].sep
		})

	def lettherebelight(self, cameratype):
		"""
		plots space
		"""
		# plots 3d space
		self.canvas = canvas(title="flock", width=1000, height=600, autoscale=False)
		# set camera position
		self.gridheight = -0.1
		self.setcamera(cameratype)
		# plots grid as ground
		self.grid_x = [curve(pos=[vector(-0.5, i, self.gridheight), vector(0.5, i, self.gridheight)]) for i in ((np.arange(11) / 10) - 0.5)]
		self.grid_y = [curve(pos=[vector(i, -0.5, self.gridheight), vector(i, 0.5, self.gridheight)]) for i in ((np.arange(11) / 10) - 0.5)]

	def proceedintime(self, duration, cameratype):
		# delete all non-curve objects (i.e. not grid) from canvas
		for canvas_object in self.canvas.objects:
			if canvas_object._objName == 'cone':
				canvas_object.visible = False
				del canvas_object
		# create cones list
		for f in self.f:
			f['cones'] = [cone(
				pos=vector(bird[0], bird[1], bird[2]),
				length=f['cone_length'],
				radius=f['cone_radius'],
				axis=vector(bird[3], bird[4], bird[5]),
				color=self.col_list[int(bird[6])]
			) for bird in f['math'].vpf]
		# update cone co-ordinates in time
		for i in range(int(duration / self.dt)):
			time.sleep(self.dt)
			for f in self.f:
				f['math'].shift(self.dt)
				for Cone in enumerate(f['cones']):	
					Cone[1].pos = vector(f['math'].vpf[Cone[0]][0], f['math'].vpf[Cone[0]][1], f['math'].vpf[Cone[0]][2])
					Cone[1].axis = vector(f['math'].vpf[Cone[0]][3], f['math'].vpf[Cone[0]][4], f['math'].vpf[Cone[0]][5])
					Cone[1].length = f['cone_length']
					Cone[1].radius = f['cone_radius']
			self.setcamera(cameratype, i)
		self.current_i += int(duration / self.dt)

	def setcamera(self, cameratype, i=0):
		"""
		sets camera position
		"""
		# vis_data = self.f[0].com_and_median_distance()
		# b = np.mean(np.abs(vis_data[1])) * 2
		# h = b / np.tan(np.pi / 6)
		# self.canvas.center = vector(0, 0, 0)
		if cameratype == 'stationary':
			pos, axis, up = self.stationary_observer(-0.2, -0.2, self.gridheight + 0.05)
		elif cameratype == 'orbiting':
			pos, axis, up = self.orbiting_observer(0.3, self.gridheight + 0.05, i)
		elif cameratype == 'bird':
			pos, axis, up = self.bird_observer(0)
		self.canvas.camera.pos = vector(pos[0], pos[1], pos[2])
		self.canvas.camera.axis = vector(axis[0], axis[1], axis[2])
		self.canvas.up = vector(up[0], up[1], up[2])

	def flat_xy_up(self, axis):
		# calculate up based on axis and a horizontal x-y plane
		k = np.array([
			axis[0] * np.cos(-np.pi / 2) - axis[1] * np.sin(-np.pi / 2), 
			axis[0] * np.sin(-np.pi / 2) + axis[1] * np.cos(-np.pi / 2),
			0])
		k = k / (np.sum(k ** 2) ** 0.5)
		return np.cos(np.pi / 2) + np.cross(k, axis) * np.sin(np.pi / 2) + k * (np.dot(k, axis)) * (1 - np.cos(np.pi / 2))

	def stationary_observer(self, x, y, z, comflockno=0):
		"""
		returns pos and axis for stationary observer fixing gaze on centre of mass
		x, y, z are x, y, z co-ordinates of observer position
		"""
		# get centre of mass
		com = self.f[comflockno]['math'].centre_of_mass()
		pos = np.array([x, y, z])
		axis = com - pos
		up = self.flat_xy_up(axis)
		return pos, axis, up

	def orbiting_observer(self, r, z, i, f=0.1, comflockno=0):
		"""
		returns pos and axis for orbiting observer fixing gaze on centre of mass
		r is distance from centre of mass
		z is height
		f is frequency of complete orbit (per second)
		"""
		com = self.f[comflockno]['math'].centre_of_mass()
		phi = 2 * np.pi * f * self.dt * (i + self.current_i)
		dz = com[2] - z
		dr = (r ** 2 - dz ** 2) ** 0.5
		x = dr * np.sin(phi)
		y = dr * np.cos(phi)
		pos = np.array([x, y, z])
		axis = com - pos
		up = self.flat_xy_up(axis)
		return pos, axis, up

	def bird_observer(self, bird, birdflockno=0):
		"""
		returns pos and axis for bird
		bird is the number bird to follow
		"""
		axis = self.f[birdflockno]['math'].v[:, 0]
		pos = self.f[birdflockno]['math'].p[:, 0] + 2 * (axis * self.f[birdflockno]['cone_length'])
		# up = self.f[0].a[:, 0]
		up = self.flat_xy_up(axis)
		return pos, axis, up

	def rotateaxes(self, i):
		"""
		rotates axes
		"""
		step = i * self.dt * 3
		x = 45 + (45 * np.sin(step))
		y = 45 + (45 * np.cos(step))
		self.ax.view_init(x, y)

	def get_data(self, flocks, i):
		"""
		get update of bird flock data every 0.2 seconds
		"""
		if i % self.integer == 0:
			data = [(j[0], j[1]) for j in [f.data() for f in flocks]]
			stringlist = ['\rflock %d; ratio: %1.2f; com: x=%1.2f, y=%1.2f, z=%1.2f; time=%1.2fs\r' % (
				d[0] + 1, d[1][0], d[1][1][0], d[1][1][1], d[1][1][2], i * self.dt) for d in enumerate(data)]
			sys.stdout.write(''.join(stringlist))
			sys.stdout.flush()

	def run(self, duration, cameratype=None):
		"""
		runs simulation
		duration is duration of simulation
		cameratype: ["orbiting", "stationary", "bird"] (default = stationary)
		"""
		if cameratype is None:
			if self.cameratype is None:
				cameratype, self.cameratype = 'stationary', 'stationary'
			else:
				cameratype = self.cameratype
		else:
			self.cameratype = cameratype
		if self.there_is_light is False:
			self.lettherebelight(cameratype)
			self.there_is_light = True
		self.proceedintime(duration, cameratype)


x = Sky(3, 0.01)
x.kunfayakuna(
	number=500, 
	attraction_exponent=1, 
	repulsion_exponent=4,
	separation=0.01, 
	alignment_exponent=0.5, 
	alignment_factor=1,
	speed=0.5)
# x.kunfayakuna(
# 	number=5, 
# 	attraction_exponent=1, 
# 	repulsion_exponent=4,
# 	separation=0.01, 
# 	alignment_exponent=1, 
# 	alignment_factor=1,
# 	speed=0.5)
x.run(
	duration=0.1,
	cameratype='orbiting')
x.kunfapredator(
	number=2,
	distance=0.8,
	prey=0,
	speed=1,
	agility_factor=40)
x.kunfapredator(
	number=3,
	distance=0.8,
	prey=0,
	speed=1,
	agility_factor=40)
x.run(duration=20)





