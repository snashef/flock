from Birds import Flock
from Birds import Sky

# set up the universe
sky = Sky(
	interval=0.001,
	size=10,
	duration=2000
)

# create some bird species
pigeons = Flock(
	number=300, 
	size=10, 
	comswivelfactor=0.1, 
	robswivelfactor=1,
	nearswivelfactor=0.2,
	velocityfactor=1,
	color='red',
	name='pigeons'
)
sparrows = Flock(
	number=50, 
	size=10, 
	comswivelfactor=0.3, 
	robswivelfactor=0,
	nearswivelfactor=0,
	velocityfactor=1.5,
	color='green',
	name='sparrows'
)
penguins = Flock(
	number=400, 
	size=10, 
	comswivelfactor=0.1, 
	robswivelfactor=0,
	nearswivelfactor=2,
	velocityfactor=1,
	color='blue',
	name='penguins'
)

# call species into existence
sky.kunfayakunu([penguins])

# # create an obstacle
# for i in range(20):
# 	sky.tree(0.5, visible=True)

# observe your creation
sky.plot(autorotate=True)



