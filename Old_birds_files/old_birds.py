import numpy as np
import time
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3

class Flock:
	def __init__(
		self, 
		number, 
		size, 
		interval, 
		robswivelfactor, 
		comswivelfactor,
		nearswivelfactor, 
		velocityfactor,
		steps
	):
		"""
		number: number of birds in flock
		size: size of space they originally occupy
		interval: time interval for animation
		comswivelfactor: rate of swivel toward centre of mass
		robswivelfactor: rate of swivel toward fixed random bird
		nearswivelfactor: rate of swivel toward nearest bird
		velocityfactor: initial bird velocity
		steps: number of steps to run animation
		e.g. birds = Flock(20, 5, 0.05, 0.2, 0.2, 0.5, 500)
		"""
		self.n = number
		self.s = size
		self.p = []
		self.v = []
		self.dt = interval
		self.rsw = robswivelfactor
		self.csw = comswivelfactor
		self.nsw = nearswivelfactor
		self.vf = velocityfactor
		self.map = np.random.randint(0, self.n, self.n)
		self.st = steps
		self.ran = np.random.random(3) * 2 * np.pi
		self.fn = 0

	def init_flock(self):
		"""
		initialises flock of birds
		matrices of position and velocity
		counts number of bird flocks 
		"""
		self.p.append((np.random.random((3, self.n)) * self.s) - (self.s / 2))
		self.v.append((np.random.random((3, self.n)) * self.s) - (self.s / 2))
		self.fn += 1

	def com(self, x, commoving, step):
		"""
		gets centre of mass
		"""
		if commoving:
			return np.array([np.sin(i + (2 * np.pi * step) / self.st) for i in self.ran])
		else:
			return np.array([(sum(i) / self.n) for i in x])

	def move(self):
		"""
		time-shift to position
		"""
		for l in range(self.fn):
			self.p[l] += self.v[l] * self.dt * self.vf

	def comswivel(self, commoving, step):
		"""
		time-shift velocity toward centre of mass
		"""
		for l in range(self.fn):
			com = self.com(self.p[l], commoving, step)
			tendto = np.swapaxes(np.stack([com] * self.n), 0, 1)
			self.v[l] += (tendto - self.p[l]) * self.csw

	def robswivel(self):
		"""
		time-shift velocity toward random fixed other bird
		"""
		for l in range(self.fn):
			tendto = self.p[l][:, self.map]
			self.v[l] += (tendto - self.p[l]) * self.rsw

	def nearswivel(self):
		"""
		time-shift velocity toward nearest bird
		"""
		for l in range(self.fn):
			tendto = self.p[l][:, self.findnearest(self.p[l])]
			self.v[l] += (tendto - self.p[l]) * self.nsw

	def findnearest(self, x):
		"""
		return mapping matrix of nearest bird
		"""
		diffs = np.array([np.subtract.outer(i, i) for i in x]) ** 2
		diffs = np.add(diffs[0], np.add(diffs[1], diffs[2]))
		# add 1000 * identity matrix to ignore self in finding nearest bird
		diffs += self.s * 1000 * np.identity(self.n)
		diffs = diffs.argmin(axis=1)
		import pdb; pdb.set_trace()  # breakpoint 052c6961 //

		return diffs

	def shift(self, rob, com, nea, commoving, step):
		"""
		how the birds move at each turn
		"""
		self.move()
		if com:
			self.comswivel(commoving, step)
		if rob:
			self.robswivel()
		if nea:
			self.nearswivel()

	def rotateaxes(self, ax, step):
		"""
		rotates axes
		"""
		step = step * 0.005
		x = (180 * np.sin(step))
		y = (180 * np.cos(step))
		ax.view_init(x, y)

	def square(self, ax):
		"""
		plots square the size of 2 * self.s
		"""
		a = self.s
		x = [-a, -a, -a, a, a, a, -a, -a, a, a, a, a, a, -a, a, -a, -a, -a]
		y = [-a, -a, a, a, a, -a, -a, a, a, -a, -a, a, -a, -a, -a, -a, a, a]
		z = [-a, a, a, a, -a, -a, -a, -a, -a, -a, a, a, a, a, a, a, a, -a]
		ax.plot(x, y, z, color='black')

	def xyz(self, ax):
		a = 1.5 * self.s
		b = a * 1.3
		c = a * 1.4
		x = [a, b, a, a, a, a]
		y = [a, a, a, b, a, a]
		z = [a, a, a, a, a, b]
		ax.plot(x, y, z, color='black')
		ax.text(c, a, a, 'x', color='gray')
		ax.text(a, c, a, 'y', color='gray')
		ax.text(a, a, c, 'z', color='gray')

	def plot(self, axis=False, square=True, xyz=True, autorotate=False, rob=False, com=True, commoving=True, nea=True):
		"""
		plots animated birds
		rob: birds follow other bird
		com: birds follow centre of mass of flock
		nea: birds follow nearest bird
		commoving: centre of mass moves
		"""
		# set up space
		fig = plt.figure(figsize=(8, 8))
		ax = fig.add_subplot(111, projection='3d')
		ax.set_xlim(-self.s * 1.5, self.s * 1.5)
		ax.set_ylim(-self.s * 1.5, self.s * 1.5)
		ax.set_zlim(-self.s * 1.5, self.s * 1.5)
		ax.grid(False)
		if axis is False:
			ax.set_axis_off()
		if square:
			self.square(ax)
		if xyz:
			self.xyz(ax)
		# plot
		birds = [None for i in range(self.fn)]
		for i in range(self.st):
			for j in birds:
				if j:
					ax.collections.remove(j)
			for j in range(self.fn):
				birds[j] = ax.scatter(self.p[j][0], self.p[j][1], self.p[j][2], marker='o', color='C%d' % j)
			if autorotate:
				self.rotateaxes(ax, i)
			plt.pause(self.dt)
			self.shift(nea=nea, rob=rob, com=com, commoving=commoving, step=i)






