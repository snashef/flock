import numpy as np
import time
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import sys
from vpython import *

class Flock:
	def __init__(
		self,
		number,
		dimensions,
		attraction_exponent,
		repulsion_exponent,
		separation,
		alignment_exponent,
		alignment_factor,
		acceleration_factor,
		predatorial_factor,
		fearful_factor,
		speed,
		colour,
		prey):
		"""
		number: number of birds in flock
		dimension: number of dimensions (2 or 3 please!)
		ground: z-value of ground (None if no ground impact)
		attraction_exponent, a: 1/r^a, exponent of attraction based on distance from neighbours, r
		repulsion_exponent, b: 1/r^b exponent of repulsion based on distance from neighbours, r
		separation: optimal distance of separation between neighbours
		alignment_exponent: c: 1/r^c exponent of velocity alignment based on distance from neighbours, r
		alignment_factor: coefficient of alignment factor wrt to separation
		acceleration_factor: coefficient of max acceleration (w.r.t. max speed)
		predatorial_factor: coefficient of predator's interest in prey (w.r.t. internal flock behaviour)
		fearful_factor: (for predators) coefficient of prey's fleeing from predator
		speed: speed birds fly
		"""
		self.n = number
		self.re = repulsion_exponent
		self.af = alignment_factor
		self.ae = attraction_exponent
		self.ale = alignment_exponent
		self.acf = acceleration_factor
		self.pred_f = predatorial_factor
		self.fearful_factor = fearful_factor
		self.d = dimensions
		self.c = colour
		self.ts = speed
		self.sep = separation
		# set repulsion factor
		self.rf = separation ** (self.re - self.ae)
		# normalise alignment factor
		multiple = (separation ** (self.ale - self.ae)) / self.ts
		self.af *= multiple
		# initialise flock
		self.p = np.random.random((self.d, self.n)) - 0.5
		self.v = np.random.random((self.d, self.n)) - 0.5
		self.a = np.zeros((self.d, self.n))
		# limit speed of each bird
		self.limit_speed()
		# predator and prey list
		self.predators = []
		self.prey = prey
		if prey:
			self.inspire_fear()
			self.choose_victim()
		# acceleration data dictionary
		self.da = {}
		# make vpython friendly data
		self.vpythonfriendly()
		# identical starting velocity
		# self.v = np.stack([self.v[:, 0] for i in range(self.n)], axis=1)

	def inspire_fear(self):
		"""
		inspire fear in prey
		"""
		self.prey.fear(self)

	def fear(self, predator):
		"""
		fear predator
		"""
		self.predators.append(predator)
		self.ff = predator.fearful_factor

	def choose_victim(self):
		"""
		if predator, choose random victim from prey flock
		"""
		self.victims = np.random.randint(low=0, high=self.prey.n, size=self.n)

	def external_predator_data(self):
		"""
		get matrices of distance from predators
		"""
		if self.predators:
			# matrix of predator positions
			p_p = np.hstack([i.p for i in self.predators])
			# matrix of displacements of each predator from each bird
			self.pred_r = np.array([np.subtract.outer(self.p[i], p_p[i]) for i in range(3)])
			# matrix of r^2
			pred_r2 = self.pred_r ** 2
			pred_r2 = np.sum(pred_r2, axis=0)
			# matrix of 1/r^2
			self.pred_invr2 = pred_r2 ** -1
			# matrix of unit vector directions to predators
			self.pred_uvec = self.pred_invr2 * self.pred_r

	def internal_flock_data(self):
		"""
		get matrices of distances between all birds
		"""
		# matrix of displacements
		self.r = np.array([np.subtract.outer(i, i) for i in self.p])
		# matrix of r^2 (distance)
		r2 = self.r ** 2
		r2 = np.sum(r2, axis=0)
		# matrix of 1/r^2
		r2 += 1000 * np.identity(self.n)
		self.invr2 = r2 ** -1	
		# matrix of unit vector directions to nearest birds
		self.uvec = self.invr2 * self.r
		# matrix of neighbours' velocities
		self.unv = np.stack([self.v for i in range(self.n)], axis=1) / self.ts

	def shift_toward_prey(self, dt):
		"""
		accelerates toward random bird from prey flock based on pred_f
		where pred_f is self.pred_f (predatorial factor)
		NOTE: no normalisation here, just (non-unit direction * pred_f) however
		should be fine because of normalisation elsewhere
		"""
		if self.prey:
			self.da['prey'] = (self.prey.p[:, self.victims] - self.p) * self.pred_f
			self.a += self.da['prey']

	def shift_away_from_predator(self, dt):
		"""
		accelerates away from predators based on c * 1/r^2
		where c is self.ff (fear factor)
		"""
		if self.predators:
			self.da['predator'] = np.sum(self.pred_invr2 * self.pred_uvec, axis=2) * dt * self.ff
			self.a += self.da['predator']

	def shift_toward_nearest_birds(self, dt):
		"""
		accelerates towards nearest birds based on a factor of 1/r^n
		where n is self.ae (attraction exponent)
		and r is distance from that bird
		"""
		self.da['attraction'] = np.sum((self.invr2 ** (self.ae * 0.5)) * self.uvec, axis=1) * dt
		self.a += self.da['attraction']

	def shift_away_from_nearest_birds(self, dt):
		"""
		accelerates away from nearest birds based on a factor of c * 1/r^n
		where n is self.re (repulsion exponent) and c is self.rf (repulsion factor)
		and r is distance from that bird
		"""
		self.da['repulsion'] = -np.sum((self.invr2 ** (self.re * 0.5)) * self.uvec, axis=1) * dt * self.rf
		self.a += self.da['repulsion']

	def align_velocity_with_nearest_birds(self, dt):
		"""
		accelerates in line with velocity of nearest birds based on a factor of c * 1/r^n
		where n is self.ae (alignment exponent) and c is self.af (alignment factor)
		and r is distance from that bird
		"""
		self.da['alignment'] = np.sum(self.invr2 ** (self.ale * 0.5) * self.unv, axis=1) * dt * self.af
		self.a += self.da['alignment']

	def limit_acceleration(self, dt):
		"""
		limits acceleration magnitude to f times the top speed
		where f is factor
		"""
		maxshiftmagnitude = self.ts * dt * self.acf
		magshift = np.sum(self.a ** 2, axis=0) ** 0.5
		magshift = np.where(magshift < maxshiftmagnitude, maxshiftmagnitude, magshift)
		self.a *= maxshiftmagnitude * magshift ** -1

	def limit_speed(self):
		"""
		limits magnitude of velocity to self.ts
		"""
		self.v *= self.ts * np.sum(self.v ** 2, axis=0) ** -0.5

	def velocityshift(self, dt):
		"""
		shift velocity based on attraction, repulsion, alignment and predators
		"""
		# calculate separation data
		self.internal_flock_data()
		self.external_predator_data()
		# calculate acceleration
		self.a = np.zeros((self.d, self.n))
		self.shift_toward_nearest_birds(dt)
		self.shift_away_from_nearest_birds(dt)
		self.align_velocity_with_nearest_birds(dt)
		self.shift_toward_prey(dt)
		self.shift_away_from_predator(dt)
		# limit acceleration magnitude
		self.limit_acceleration(dt)
		# shift velocity
		self.v += self.a
		# limit speed
		self.limit_speed()
		# predator shift
		# if len(self.predators) > 0:
		# 	# get predator data
		# 	self.predator_data()

	def positionshift(self, dt):
		"""
		shift position based on current velocity
		"""
		self.p += self.v * dt

	def shift(self, dt):
		"""
		general position-velocity shift
		"""
		self.positionshift(dt)
		self.velocityshift(dt)
		self.vpythonfriendly()

	def vpythonfriendly(self):
		"""
		update vpython friendly data
		"""
		self.vpf = np.append(np.swapaxes(self.p, 0, 1), np.swapaxes(self.v, 0, 1), axis=1)
		colors = np.array([[self.c] for i in range(self.n)])
		self.vpf = np.append(self.vpf, colors, axis=1)

	def data(self):
		"""
		returns data on flock at current moment in time:
		ratio of shifts
		centre of mass
		"""
		# check ratio of shifts
		gvs_mag = np.mean(np.sum(self.avs ** 2, axis=0) ** 0.5)
		svs_mag = np.mean(np.sum(self.rvs ** 2, axis=0) ** 0.5)
		avs_mag = np.mean(np.sum(self.alvs ** 2, axis=0) ** 0.5)
		ratio = np.mean([gvs_mag, svs_mag]) / avs_mag
		# check centre of mass
		com = np.mean(self.p, axis=1)
		return ratio, com

	def median_distance(self, com=None):
		"""
		returns median distance from centre of mass
		"""
		if com is None:
			com = np.mean(self.p, axis=1)
		vector_med = np.median(np.abs(self.p - np.array([com]).swapaxes(0, 1)), axis=1)		
		return np.sum(vector_med ** 2) ** 0.5

	def centre_of_mass(self):
		"""
		returns centre of mass
		"""
		return np.mean(self.p, axis=1)

class Sky:
	def __init__(
		self,
		dimensions,
		interval):
		"""
		dimensions: number of dimensions (2 or 3 please!)
		interval: size of smallest time unit (in seconds)
		duration: how long the sky lasts (in seconds)
		"""
		self.d = dimensions
		self.dt = interval
		# empty list of bird flocks
		self.f = []
		# integer for every two second data
		self.integer = int(0.2 / self.dt)
		# colour dictionary
		self.col_list = [vec(1, 1, 0), vec(1, 0, 0), vec(0, 1, 0), vec(1, 0, 1)]
		# there is light
		self.there_is_light = False
		# camera type
		self.cameratype = None
		# current i
		self.current_i = 0
		# optimum observer camera data
		self.oo_data = {'pos': [], 'axis': [], 'up': []}

	def kunfayakuna(
		self, number, attraction_exponent, repulsion_exponent, 
		separation, alignment_exponent, alignment_factor, 
		acceleration_factor, speed, predatorial_factor=None, 
		fearful_factor=None, prey=None):
		"""
		incorporate bird flock
		number: number of birds in flock
		dimension: number of dimensions (2 or 3 please!)
		ground: z-value of ground (None if no ground impact)
		attraction_exponent, a: 1/r^a, exponent of attraction based on distance from neighbours, r
		repulsion_exponent, b: 1/r^b exponent of repulsion based on distance from neighbours, r
		separation: optimal distance of separation between neighbours
		alignment_exponent: c: 1/r^c exponent of velocity alignment based on distance from neighbours, r
		alignment_factor: coefficient of alignment factor wrt to separation
		acceleration_factor: coefficient of max acceleration (w.r.t. max speed)
		predatorial_factor: coefficient of predator's interest in prey (w.r.t. internal flock behaviour)
		fearful_factor: (for predators) coefficient of prey's fleeing from predator
		speed: speed birds fly
		ground: z-value of ground (None of no ground)
		"""
		colourindex = len(self.f)
		prey = self.f[prey]['math'] if prey is not None else None
		self.f.append({
			'math': Flock(
				number, self.d, attraction_exponent, repulsion_exponent, separation, 
				alignment_exponent, alignment_factor, acceleration_factor, predatorial_factor,
				fearful_factor, speed, colourindex, prey),
			'cone_length': separation,
			'cone_radius': separation / 5})

	def lettherebelight(self, cameratype):
		"""
		plots space
		"""
		# plots 3d space
		self.canvas = canvas(title="flock", width=1000, height=600, autoscale=False)
		# set camera position
		self.gridheight = -0.1
		self.setcamera(cameratype)
		# plots grid as ground
		grid_range = 10
		grid_interval = 0.1
		self.grid_x = [curve(pos=[vector(-grid_range / 2, i, self.gridheight), vector(grid_range / 2, i, self.gridheight)]) for i in ((np.arange(int(grid_range / grid_interval) + 1) * grid_interval) - (grid_range / 2))]
		self.grid_y = [curve(pos=[vector(i, -grid_range / 2, self.gridheight), vector(i, grid_range / 2, self.gridheight)]) for i in ((np.arange(int(grid_range / grid_interval) + 1) * grid_interval) - (grid_range / 2))]

	def proceedintime(self, duration, cameratype, counter, forces, positions, camera):
		# delete all non-curve objects (i.e. not grid) from canvas
		for canvas_object in self.canvas.objects:
			if canvas_object._objName == 'cone':
				canvas_object.visible = False
				del canvas_object
		# create cones list
		for f in self.f:
			f['cones'] = [cone(
				pos=vector(bird[0], bird[1], bird[2]),
				size=vector(f['cone_length'], f['cone_radius'] * 0.4, f['cone_radius'] * 1.5),
				axis=vector(bird[3], bird[4], bird[5]),
				color=self.col_list[int(bird[6])]
			) for bird in f['math'].vpf]
		# update cone co-ordinates in time
		for i in range(int(duration / self.dt)):
			# print data to terminal
			self.terminal_data(counter, forces, positions, camera, i)
			time.sleep(self.dt)
			for f in self.f:
				f['math'].shift(self.dt)
				for Cone in enumerate(f['cones']):	
					Cone[1].pos = vector(f['math'].vpf[Cone[0]][0], f['math'].vpf[Cone[0]][1], f['math'].vpf[Cone[0]][2])
					Cone[1].axis = vector(f['math'].vpf[Cone[0]][3], f['math'].vpf[Cone[0]][4], f['math'].vpf[Cone[0]][5])
					Cone[1].size = vector(f['cone_length'], f['cone_radius'] * 0.4, f['cone_radius'] * 1.5)
			self.setcamera(cameratype, i)
			# print(np.mean(self.f[0]['math'].p[2, :]), np.std(self.f[0]['math'].p[2, :]))
		self.current_i += int(duration / self.dt)

	def terminal_data(self, counter, forces, positions, camera, i):
		"""
		prints counter, positions and forces data to terminal 
		(positions and forces data of first flock)
		"""
		CURSER_UP_ONE = '\x1b[1A'
		ERASE_LINE = '\x1b[2K'
		string = ''
		if counter:
			# every 0.01 seconds
			if i % int(0.01 / self.dt) == 0:
				self.counter_string = '\rtime: %1.2fs\n' % ((i + self.current_i) * self.dt)
			string += self.counter_string
			sys.stdout.write(CURSER_UP_ONE + ERASE_LINE)
		if camera:
			# every 0.1 seconds
			if i % int(0.1 / self.dt) == 0:
				pos = '%1.2f,%1.2f,%1.2f' % (self.canvas.camera.pos.x, self.canvas.camera.pos.y, self.canvas.camera.pos.z)
				axis = '%1.2f,%1.2f,%1.2f' % (self.canvas.camera.axis.x, self.canvas.camera.axis.y, self.canvas.camera.axis.z)
				up = '%1.2f,%1.2f,%1.2f' % (self.canvas.up.x, self.canvas.up.y, self.canvas.up.z)
				self.camera_string = 'camera pos: %s    axis: %s    up: %s\n' % (pos, axis, up)
			string += self.camera_string
			sys.stdout.write(CURSER_UP_ONE + ERASE_LINE)
		if positions:
			# every 0.1 seconds
			if i % int(0.1 / self.dt) == 0:
				centre_of_mass = '%1.2f,%1.2f,%1.2f' % (tuple([j for j in self.f[0]['math'].centre_of_mass()]))
				self.positions_string = 'com: %s    median distance from com: %1.2f\n' % (
					centre_of_mass, self.f[0]['math'].median_distance())
			string += self.positions_string
			sys.stdout.write(CURSER_UP_ONE + ERASE_LINE)
		if forces:
			# every 0.1 seconds
			if i % int(0.1 / self.dt) == 0:
				self.forces_string = '   '.join(['%s: %1.2f' % (j[0], np.sum(np.sum(j[1] ** 2, axis=1)) ** 0.5) for j in self.f[0]['math'].da.items()])
			string += self.forces_string
		sys.stdout.write(string + '\r')
		sys.stdout.flush()

	def setcamera(self, cameratype, i=0):
		"""
		sets camera position
		"""
		# vis_data = self.f[0].com_and_median_distance()
		# b = np.mean(np.abs(vis_data[1])) * 2
		# h = b / np.tan(np.pi / 6)
		# self.canvas.center = vector(0, 0, 0)
		if cameratype == 'stationary':
			pos, axis, up = self.stationary_observer(-0.2, -0.2, self.gridheight + 0.05)
			# pos, axis, up = self.stationary_observer(-0.5, -0.5, self.gridheight + 0.05)
		elif cameratype == 'orbiting':
			pos, axis, up = self.orbiting_observer(0.3, self.gridheight + 0.05, i)
		elif cameratype == 'bird':
			pos, axis, up = self.bird_observer(0)
		elif cameratype == 'optimum':
			pos, axis, up = self.optimum_observer(self.gridheight + 0.05, i)
		self.canvas.camera.pos = vector(pos[0], pos[1], pos[2])
		self.canvas.camera.axis = vector(axis[0], axis[1], axis[2])
		self.canvas.up = vector(up[0], up[1], up[2])

	def flat_xy_up(self, axis):
		# calculate up based on axis and a horizontal x-y plane
		k = np.array([
			axis[0] * np.cos(-np.pi / 2) - axis[1] * np.sin(-np.pi / 2), 
			axis[0] * np.sin(-np.pi / 2) + axis[1] * np.cos(-np.pi / 2),
			0])
		k = k / (np.sum(k ** 2) ** 0.5)
		return np.cos(np.pi / 2) + np.cross(k, axis) * np.sin(np.pi / 2) + k * (np.dot(k, axis)) * (1 - np.cos(np.pi / 2))

	def stationary_observer(self, x, y, z, comflockno=0):
		"""
		returns pos and axis for stationary observer fixing gaze on centre of mass
		x, y, z are x, y, z co-ordinates of observer position
		"""
		# get centre of mass
		com = self.f[comflockno]['math'].centre_of_mass()
		pos = np.array([x, y, z])
		axis = com - pos
		up = self.flat_xy_up(axis)
		return pos, axis, up

	def optimum_observer(self, z, i, comflockno=0):
		"""
		returns pos and axis for optimum observer fixing gaze on centre of mass and following flock
		remaining on the y axis, updating every 0.1s
		"""
		com = self.f[comflockno]['math'].centre_of_mass()
		median = self.f[comflockno]['math'].median_distance(com)
		x = 2 * (3 ** 0.5) * median		
		self.oo_data['pos'].append(np.array([x, com[1], z]))
		self.oo_data['axis'].append(com - self.oo_data['pos'][-1])
		self.oo_data['up'].append(self.flat_xy_up(self.oo_data['axis'][-1]))
		smoothingperiod = 1
		meaninterval = int(smoothingperiod / self.dt)
		if len(self.oo_data['pos']) > meaninterval:
			for datum in ['pos', 'axis', 'up']:
				self.oo_data[datum] = self.oo_data[datum][-meaninterval:]
		return np.mean(self.oo_data['pos'], axis=0), np.mean(self.oo_data['axis'], axis=0), np.mean(self.oo_data['up'], axis=0)

		# if i % int(0.01 / self.dt) == 0:
		# 	for datum in ['pos', 'axis', 'up']:
		# 		self.oo_data['prev_' + datum] = np.mean(self.oo_data[datum], axis=0)
		# 		self.oo_data[datum] = []
		# return self.oo_data['prev_pos'], self.oo_data['prev_axis'], self.oo_data['prev_up']

	def orbiting_observer(self, r, z, i, f=0.1, comflockno=0):
		"""
		returns pos and axis for orbiting observer fixing gaze on centre of mass
		r is distance from centre of mass
		z is height
		f is frequency of complete orbit (per second)
		"""
		com = self.f[comflockno]['math'].centre_of_mass()
		phi = 2 * np.pi * f * self.dt * (i + self.current_i)
		dz = com[2] - z
		dr = (r ** 2 - dz ** 2) ** 0.5
		x = dr * np.sin(phi)
		y = dr * np.cos(phi)
		pos = np.array([x + com[0], y + com[1], z])
		axis = com - pos
		up = self.flat_xy_up(axis)
		return pos, axis, up

	def bird_observer(self, bird, birdflockno=0):
		"""
		returns pos and axis for bird
		bird is the number bird to follow
		"""
		axis = self.f[birdflockno]['math'].v[:, 0]
		pos = self.f[birdflockno]['math'].p[:, 0] + 2 * (axis * self.f[birdflockno]['cone_length'])
		# up = self.f[0].a[:, 0]
		up = self.flat_xy_up(axis)
		return pos, axis, up

	def get_data(self, flocks, i):
		"""
		get update of bird flock data every 0.2 seconds
		"""
		if i % self.integer == 0:
			data = [(j[0], j[1]) for j in [f.data() for f in flocks]]
			stringlist = ['\rflock %d; ratio: %1.2f; com: x=%1.2f, y=%1.2f, z=%1.2f; time=%1.2fs\r' % (
				d[0] + 1, d[1][0], d[1][1][0], d[1][1][1], d[1][1][2], i * self.dt) for d in enumerate(data)]
			sys.stdout.write(''.join(stringlist))
			sys.stdout.flush()

	def run(self, duration, cameratype=None, counter=True, forces=True, positions=True, camera=True):
		"""
		runs simulation
		duration is duration of simulation
		cameratype: ["orbiting", "stationary", "bird"] (default = stationary)
		"""
		if cameratype is None:
			if self.cameratype is None:
				cameratype, self.cameratype = 'stationary', 'stationary'
			else:
				cameratype = self.cameratype
		else:
			self.cameratype = cameratype
		if self.there_is_light is False:
			self.lettherebelight(cameratype)
			self.there_is_light = True
		self.proceedintime(duration, cameratype, counter, forces, positions, camera)


x = Sky(3, 0.002)
x.kunfayakuna(
	number=200, 
	attraction_exponent=1, 
	repulsion_exponent=4,
	separation=0.01, 
	alignment_exponent=0.5, 
	alignment_factor=0.5,
	acceleration_factor=10,
	speed=0.5)
# x.kunfayakuna(
# 	number=5, 
# 	attraction_exponent=1, 
# 	repulsion_exponent=4,
# 	separation=0.01, 
# 	alignment_exponent=1, 
# 	alignment_factor=1,
# 	speed=0.5)
x.run(
	duration=10,
	cameratype='optimum')
x.kunfayakuna(
	number=1, 
	attraction_exponent=1, 
	repulsion_exponent=4,
	separation=0.05, 
	alignment_exponent=0.5, 
	alignment_factor=1,
	acceleration_factor=5,
	predatorial_factor=1000,
	fearful_factor=5,
	speed=0.5,
	prey=0)
# x.kunfayakuna(
# 	number=3, 
# 	attraction_exponent=1, 
# 	repulsion_exponent=4,
# 	separation=0.02, 
# 	alignment_exponent=0.5, 
# 	alignment_factor=1,
# 	acceleration_factor=10,
# 	predatorial_factor=1000,
# 	fearful_factor=1,
# 	speed=0.5,
# 	prey=0)
x.run(duration=20)





