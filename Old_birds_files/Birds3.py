import numpy as np
import time
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import sys

class Flock:
	def __init__(
		self,
		number,
		dimensions,
		attraction_exponent,
		repulsion_exponent,
		separation,
		alignment_exponent,
		alignment_factor,
		speed,
		colour):
		"""
		number: number of birds in flock
		dimension: number of dimensions (2 or 3 please!)
		attraction_exponent, a: 1/r^a, exponent of attraction based on distance from neighbours, r
		repulsion_exponent, b: 1/r^b exponent of repulsion based on distance from neighbours, r
		separation: optimal distance of separation between neighbours
		alignment_exponent: c: 1/r^c exponent of velocity alignment based on distance from neighbours, r
		alignment_factor: coefficient of alignment factor wrt to separation
		speed: speed birds fly
		"""
		self.n = number
		self.re = repulsion_exponent
		self.af = alignment_factor
		self.ae = attraction_exponent
		self.ale = alignment_exponent
		self.d = dimensions
		self.c = colour
		self.ts = speed
		# set repulsion factor
		self.rf = (self.ae / self.re) * (separation ** (self.re - self.ae))
		# normalise alignment factor
		multiple = separation ** (self.ale - self.ae) - self.rf * separation ** (self.ale - self.re)		
		self.af *= multiple
		# initialise flock
		self.p = np.random.random((self.d, self.n)) - 0.5
		self.v = np.random.random((self.d, self.n)) - 0.5
		# limit speed of each bird
		self.v *= self.ts * np.sum(self.v ** 2, axis=0) ** -0.5
		# identical starting velocity
		# self.v = np.stack([self.v[:, 0] for i in range(self.n)], axis=1)

	def positionshift(self, dt):
		"""
		shift position based on current velocity
		"""
		self.p += self.v * dt

	def separation_data(self):
		"""
		get matrices of distances between all birds
		"""
		# matrix of displacements
		self.r = np.array([np.subtract.outer(i, i) for i in self.p])
		# matrix of r^2 (distance)
		r2 = self.r ** 2
		r2 = np.sum(r2, axis=0)
		# matrix of 1/r^2
		self.invr2 = np.where(r2 == 0, 0, r2 ** -1)		
		# matrix of unit vector directions to nearest birds
		self.uvec = self.invr2 * self.r
		# matrix of neighbours' velocities
		self.unv = np.stack([self.v for i in range(self.n)], axis=1) / self.ts

	def velocityshift(self, dt):
		"""
		shift velocity based on:
		glue factor (tend to nearest neighbour based on r^-2)
		separation factor (tend away from nearest neighbour based on r^-6)
		velocity factor (tend to nearest neighbour's velocity)
		"""
		# get separation data
		self.separation_data()
		# tend to nearest birds (based on 1/r^a)
		self.avs = np.sum((self.invr2 ** (self.ae * 0.5)) * self.uvec, axis=1) * dt
		# separation shift (based on 1/r^b)
		self.rvs = -np.sum((self.invr2 ** (self.re * 0.5)) * self.uvec, axis=1) * dt * self.rf
		# alignment shift (based on 1/r)
		self.alvs = np.sum(self.invr2 ** (self.ale * 0.5) * self.unv, axis=1) * dt * self.af
		# limit shift (10 * greater than speed limit) then shift
		shift = self.avs + self.rvs + self.alvs
		maxshiftmagnitude = self.ts * dt * 10
		magshift = np.sum(shift ** 2, axis=0) ** 0.5
		magshift = np.where(magshift < maxshiftmagnitude, maxshiftmagnitude, magshift)
		shift *= maxshiftmagnitude * magshift ** -1
		self.v += shift
		# limit speed of each bird
		speedsquared = np.sum(self.v ** 2, axis=0)
		self.v *= self.ts * speedsquared ** -0.5

	def shift(self, dt):
		"""
		general position-velocity shift
		"""
		self.positionshift(dt)
		self.velocityshift(dt)

	def data(self):
		"""
		returns data on flock at current moment in time:
		ratio of shifts
		centre of mass
		"""
		# check ratio of shifts
		gvs_mag = np.mean(np.sum(self.avs ** 2, axis=0) ** 0.5)
		svs_mag = np.mean(np.sum(self.rvs ** 2, axis=0) ** 0.5)
		avs_mag = np.mean(np.sum(self.alvs ** 2, axis=0) ** 0.5)
		ratio = np.mean([gvs_mag, svs_mag]) / avs_mag
		# check centre of mass
		com = np.mean(self.p, axis=1)
		return ratio, com

class Sky:
	def __init__(
		self,
		dimensions,
		interval):
		"""
		dimensions: number of dimensions (2 or 3 please!)
		interval: size of smallest time unit (in seconds)
		duration: how long the sky lasts (in seconds)
		"""
		self.d = dimensions
		self.dt = interval
		# empty list of bird flocks
		self.f = []
		# integer for every two second data
		self.integer = int(0.2 / self.dt)

	def kunfayakuna(self, number, attraction_exponent, repulsion_exponent, separation, alignment_exponent, alignment_factor, speed):
		"""
		incorporate bird flock
		number: number of birds in flock
		attraction_exponent, a: 1/r^a, exponent of attraction based on distance from neighbours, r
		repulsion_exponent, b: 1/r^b exponent of repulsion based on distance from neighbours, r
		separation: optimal distance of separation between neighbours
		alignment_exponent: c: 1/r^c exponent of velocity alignment based on distance from neighbours, r
		alignment_factor: coefficient of alignment factor wrt to separation
		speed: speed birds fly
		"""
		colour = ['b', 'r', 'g', 'm', 'c']
		colourindex = len(self.f)
		self.f.append(Flock(number, self.d, attraction_exponent, repulsion_exponent, separation, alignment_exponent, alignment_factor, speed, colour[colourindex]))

	def lettherebelight(self):
		"""
		plots space
		"""
		# plot space in 2 or 3 dimensions
		size = 0.2
		fig = plt.figure(figsize=(8, 8))
		if self.d == 2:
			self.ax = fig.add_subplot(111)
		else:
			self.ax = fig.add_subplot(111, projection='3d')
		self.ax.set_xlim(-size, size)
		self.ax.set_ylim(-size, size)
		if self.d == 3:
			self.ax.set_zlim(-size, size)
		# turn off axes
		# self.ax.set_axis_off()

	def proceedintime(self, interval):
		scatters = []
		# run for certain period
		for i in range(int(interval / self.dt)):
			# remove previous plot
			for scatter in scatters:
				self.ax.collections.remove(scatter)
			# add new plot
			if self.d == 3:
				scatters = [self.ax.scatter(x.p[0], x.p[1], x.p[2], marker='o', color=x.c) for x in self.f]
			else:
				scatters = [self.ax.scatter(x.p[0], x.p[1], marker='o', color=x.c) for x in self.f]
			# rotate axes
			self.rotateaxes(i)
			# pause
			plt.pause(self.dt)
			# time-alter bird flock
			for f in self.f:
				f.shift(self.dt)
			# get data from bird flock
			self.get_data(self.f, i)

	def rotateaxes(self, i):
		"""
		rotates axes
		"""
		step = i * self.dt * 3
		x = 45 + (45 * np.sin(step))
		y = 45 + (45 * np.cos(step))
		self.ax.view_init(x, y)

	def get_data(self, flocks, i):
		"""
		get update of bird flock data every 0.2 seconds
		"""
		if i % self.integer == 0:
			data = [(j[0], j[1]) for j in [f.data() for f in flocks]]
			stringlist = ['\rflock %d; ratio: %1.2f; com: x=%1.2f, y=%1.2f, z=%1.2f; time=%1.2fs\r' % (
				d[0] + 1, d[1][0], d[1][1][0], d[1][1][1], d[1][1][2], i * self.dt) for d in enumerate(data)]
			sys.stdout.write(''.join(stringlist))
			sys.stdout.flush()

	def run(self, duration):
		self.lettherebelight()
		self.proceedintime(duration)


x = Sky(3, 0.01)
x.kunfayakuna(
	number=200, 
	attraction_exponent=1, 
	repulsion_exponent=2,
	separation=0.01, 
	alignment_exponent=1, 
	alignment_factor=1,
	speed=1)
# x.kunfayakuna(10, 1, 1, 1, 1)
x.run(duration=10)





