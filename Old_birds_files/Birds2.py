import numpy as np
import time
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

class Flock:
	def __init__(
		self,
		number,
		size,
		separationfactor,
		alignmentfactor,
		cohesionfactor,
		velocityfactor,
		name,
		color=None
	):
		"""
		number: number of birds in flock
		size: size of space they originally occupy
		comswivelfactor: rate of swivel toward centre of mass
		robswivelfactor: rate of swivel toward fixed random bird
		nearswivelfactor: rate of swivel toward nearest bird
		velocityfactor: general magnitude of velocities wrt size of flock
		e.g. birds = Flock(
			number=20, 
			size=5, 
			robswivelfactor=0, 
			comswivelfactor=0.1, 
			nearswivelfactor=1, 
			velocityfactor=0.5
		)
		"""
		self.n = number
		self.s = size
		self.p = np.zeros((3, self.n))
		self.v = np.zeros((3, self.n))
		self.sf = separationfactor
		self.als = alignmentfactor
		self.cf = cohesionfactor
		self.vf = velocityfactor
		self.rm = np.random.randint(0, self.n, self.n)
		self.name = name
		self.c = color

	def init_flock(self):
		"""
		randomly initialises bird flock positions and velocities
		"""
		self.p = (np.random.random((3, self.n)) * self.s) - (self.s / 2)
		self.v = (np.random.random((3, self.n)) * self.s) - (self.s / 2)

	def move(self, dt):
		"""
		time-shift position
		"""
		self.p += self.v * dt * self.vf

	def com(self):
		"""
		returns centre of mass of position self.p
		"""
		return np.array([(sum(i) / self.n) for i in self.p])

	def averagevelocity(self):
		"""
		returns average velocity of self.v
		"""
		return np.mean(self.v, axis=1).reshape((3, 1))

	def distances(self):
		"""
		get matrix of distances between all birds
		"""
		diffs = np.array([np.subtract.outer(i, i) for i in self.p]) ** 2
		diffs = np.add(diffs[0], np.add(diffs[1], diffs[2]))
		# add 1000 * identity matrix to ignore self in finding nearest bird
		diffs += self.s * 1000 * np.identity(self.n)
		return diffs

	def findnearest(self):
		"""
		return two objects:
		mapping matrix of nearest bird
		distance to that bird
		"""
		diffs = self.distances()
		nearests = diffs.argmin(axis=1)		
		return nearests, diffs[np.arange(self.n), nearests]

	def separation(self):
		"""
		steer away from neighbour who is too close
		"""
		nearests, distances = self.findnearest()
		nearest = self.p[:, nearests]
		displacement = self.p - nearest
		sepshift = displacement * (1 / distances) * self.sf
		self.v += sepshift

	def alignment(self):
		"""
		steer in general direction of flock
		"""
		alishift = (self.averagevelocity() - self.v) * self.als
		self.v += alishift

	def cohesion(self):
		"""
		steer toward flock centre of mass
		"""
		com = self.com()
		tendto = np.swapaxes(np.stack([com] * self.n), 0, 1)
		self.v += (tendto - self.p) * self.cf

	def shift(self, dt):
		"""
		total time-shift of position and velocity
		"""
		self.move(dt)
		self.separation()
		self.alignment()
		self.cohesion()

class Sky:
	def __init__(
		self,
		interval,
		size,
		duration
	):
		"""
		interval: size of smallest time unit (in seconds)
		size: size of dimensions
		duration: number of steps animation runs for
		e.g. sky = Sky(
			interval=0.01,
			size=7,
			duration = 500
		)
		"""
		self.dt = interval
		self.s = size
		self.N = duration
		self.b = []  # empty set to be filled with flock instances
		self.n = 0  # number of flocks of birds in sky

	def kunfayakunu(self, x):
		"""
		incorporate Flock instance (or list of instances) into Sky
		e.g. sky.kunfayakunu('pigeon')
		"""
		if not isinstance(x, list):
			print('ERROR: must call beings into existence as list')
		for i in x:
			if isinstance(i, Flock):
				# acknowledge
				self.b.append(i)
				# initialise
				i.init_flock()
				# count
				self.n += 1

	def plot(self, axis=False, cube=True, xyz=True, autorotate=True, legend=True):
		"""
		plots space
		"""
		# set up space
		fig = plt.figure(figsize=(8, 8))
		ax = fig.add_subplot(111, projection='3d')
		ax.set_xlim(-self.s * 1.5, self.s * 1.5)
		ax.set_ylim(-self.s * 1.5, self.s * 1.5)
		ax.set_zlim(-self.s * 1.5, self.s * 1.5)
		ax.grid(False)
		# visual aids
		if axis is False:
			ax.set_axis_off()
		if cube:
			self.cube(ax)
		if xyz:
			self.xyz(ax)
		# plot
		b_scatter = [None for j in range(self.n)]
		for i in range(self.N):
			# remove previous plot
			for j in b_scatter:
				if j:
					ax.collections.remove(j)
			# plot new plot
			for j in range(self.n):
				x = self.b[j].p
				color = self.b[j].c
				color = color if color else 'C%d' % j
				name = self.b[j].name
				b_scatter[j] = ax.scatter(
					x[0], x[1], x[2], 
					marker='o', 
					color=color,
					label=name)
			# legend
			if legend:
				ax.legend(loc=1)
			# pause
			plt.pause(self.dt)
			# rotate axes
			if autorotate:
				self.rotateaxes(ax, i)
			# shift bird flock
			for j in range(self.n):
				self.b[j].shift(self.dt)

	def cube(self, ax):
		"""
		plots transparent cube
		"""
		# cube vertices
		a = self.s
		x = [[a * (-1) ** (i // j) for j in (1, 2, 4)] for i in range(8)]
		# cube sides
		s = [
			[x[i] for i in (0, 1, 3, 2)],
			[x[i] for i in (4, 5, 7, 6)],
			[x[i] for i in (0, 2, 6, 4)],
			[x[i] for i in (1, 3, 7, 5)],
			[x[i] for i in (0, 1, 5, 4)],
			[x[i] for i in (2, 3, 7, 6)]
		]
		collection = Poly3DCollection(
			s, edgecolors='black', linewidths=0.5, alpha=0.2)
		collection.set_facecolor([1, 1, 1])
		ax.add_collection3d(collection)

	def xyz(self, ax):
		"""
		plots off-centre small x-y-z axes
		"""
		a = 1.5 * self.s
		b = a * 1.3
		c = a * 1.4
		x = [a, b, a, a, a, a]
		y = [a, a, a, b, a, a]
		z = [a, a, a, a, a, b]
		ax.plot(x, y, z, color='black')
		ax.text(c, a, a, 'x', color='gray')
		ax.text(a, c, a, 'y', color='gray')
		ax.text(a, a, c, 'z', color='gray')

	def rotateaxes(self, ax, step):
		"""
		rotates axes
		"""
		step = step * 0.005
		x = (180 * np.sin(step))
		y = (180 * np.cos(step))
		ax.view_init(x, y)






