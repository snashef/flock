from Birds2 import Flock
from Birds2 import Sky

# set up the universe
sky = Sky(
	interval=0.001,
	size=1,
	duration=2000
)

# create some bird species
penguins = Flock(
	number=100, 
	size=1, 
	separationfactor=0.05,
	alignmentfactor=0.05,
	cohesionfactor=0.1,
	velocityfactor=10,
	color='blue',
	name='penguins'
)

# call species into existence
sky.kunfayakunu([penguins])

# observe your creation
sky.plot(autorotate=True, cube=True, xyz=True)



